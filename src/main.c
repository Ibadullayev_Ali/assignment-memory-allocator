#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>

void test1();
void test2();
void test3();
void test4();
void test5();

void* heap = NULL;
//here I initialize a Heap 
void heap_initialize(size_t size) {
    heap = heap_init(size);
    debug_heap(stdout, heap);
    fprintf(stdout, "The heap is initialized\n\n");
}

//Обычное успешное выделение памяти.
void test1() {
    fprintf(stdout, "\nTest 1 started!\n\n");
    void* temp = _malloc(1000);

    if (!temp) {
        err("Error in test 1!\n\n");
    }

    debug_heap(stdout, heap);
    _free(temp);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nTest 1 passed!\n\n");
}

//Освобождение одного блока из нескольких выделенных.
void test2() {
    fprintf(stdout, "\nTest 2 started!\n\n");
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(2000);

    if (!temp1 || !temp2) {
        err("Error in test 2!\n\n");
    }

    debug_heap(stdout, heap);
    fprintf(stdout, "\nRelease of the first block...\n");
    _free(temp1);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nTest 2 passed!\n\n");
    _free(temp2);
}

//Освобождение двух блоков из нескольких выделенных.
void test3() {
    fprintf(stdout, "\nTest 3 started!\n\n");
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(1500);
    void* temp3 = _malloc(3000);

    if (!temp1 || !temp2 || !temp3) {
        err( "Error in test 3!\n\n");
    }

    debug_heap(stdout, heap);
    fprintf(stdout, "\nRelease of the first block...\n");
    _free(temp1);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nRelease of the second block...\n");
    _free(temp2);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nTest 3 passed!\n\n");
    _free(temp3);
}

//Память закончилась, новый регион памяти расширяет старый.
void test4() {
    fprintf(stdout, "\nTest 4 started!\n\n");
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(2000);
    void* temp3 = _malloc(10000);

    if (!temp1 || !temp2 || !temp3) {
        err( "Error in test 4!\n\n");
    }

    debug_heap(stdout, heap);
    fprintf(stdout, "\nTest 4 passed!\n\n");
    _free(temp1);
    _free(temp2);
    _free(temp3);
}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте
void test5() {
    fprintf(stdout, "\nTest 5 started!\n\n");
    void* temp1 = _malloc(8000);
    void* temp2 = _malloc(20000);
    void* temp3 = _malloc(100000);

    if (!temp1 || !temp2 || !temp3) {
        err( "Error in test 5!\n\n");
    }

    debug_heap(stdout, heap);
    fprintf(stdout, "\nTest 5 passed!\n\n");
    _free(temp1);
    _free(temp2);
    _free(temp3);
}

int main() {
    heap_initialize(10000);
    test1();
    test2();
    test3();
    test4();
    test5();
    fprintf(stdout, "All tests passed successfully!");
    return 0;
}
